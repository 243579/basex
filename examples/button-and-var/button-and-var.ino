#include <basex.h>

/* This code expects to have a push-button which grounds pin D3 (yes!) when pushed
 * It has an int state-variable (MQTT controllable), which is incremented by 1 every press, and is cleared on a long press
 * But we can do more: Double click, only-short-click, ...
 * Every here and there the FPS is printed on the Serial. Target is 100 FPS
 * Tested on ESP8266, (Wemos D1 Mini)
 */

MAKE_BUTTON(button5, D8, "button/ack");

IntVar v1(0, "/var/cmd", "/var/ack");

void setup() {
  LoopManager::instance().connect(button5, Signal_t::PRESSED, v1, Slot_t::INCREMENT);
  LoopManager::instance().connect(button5, Signal_t::HOLD   , v1, Slot_t::RESET    );
  LoopManager::instance().setup("demo-device", "wifi_name", "wifi_pass", "mqtt_broker_ip");
}

void loop() {
  LoopManager::instance().loop(); 
}
















