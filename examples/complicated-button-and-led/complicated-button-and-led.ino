/*
 * This is the "Blink" example of  BaseX
 * We have a blinking LED (some cool modes supported) connected between D8 and 3.3V (Just stick it in...)
 * A pushbutton that grounds D3 to GND will switch LED blinking mode (OFF, ON, BLINK, BLINK2, DECAY, TRIG)
 * A double-click on the button will reset LED to OFF mode
 * A long click on the button will change brightness of the LED
 * We can also control blinking mode with an MQTT command. It is also reported on such
 */

#include <basex.h>

LedConfig lc(LedConfig::BLINK2);
BlinkyLed bled(15, lc); // Stick a LED between D8 and 3.3V
IntVar i(0, "i/cmd", "i/ack");
MAKE_BUTTON(button, D8, "but/ack");
BoolVar inHold(false);  // Tracks if the button is in hold or not

class Stam : public Object {
  void increment(Object *pOther) {
    lc.style = (uint8) i.value();
  }
  void decrement(Object *pOther) {
    lc.style = LedConfig::OFF;
  }
} stam;

void setup() {
  LoopManager::instance().connect(i, Signal_t::CHANGED, stam, Slot_t::INCREMENT);
  LoopManager::instance().connect(button, Signal_t::CLICK_ONLY, i, Slot_t::INCREMENT);
  LoopManager::instance().connect(button, Signal_t::DOUBLE_CLICK, i, Slot_t::RESET);
  LoopManager::instance().connect(button, Signal_t::HOLD    , inHold, Slot_t::INCREMENT);
  LoopManager::instance().connect(button, Signal_t::HOLD_END, inHold, Slot_t::DECREMENT);
  LoopManager::instance().setup("device", "wifi_name", "wifi_pass", "mqtt_broker_ip", 2);
  i.setLimits(LedConfig::OFF, LedConfig::TRANS, WRAP_AROUND);
}

void loop() {
  LoopManager::instance().loop();
  if (inHold.value()) {
    bled.targetBrightness = (bled.targetBrightness + 1) % 1024;
  }
}
