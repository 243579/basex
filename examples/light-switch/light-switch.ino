/*
 * This is a simple example of an MQTT controlled light switch
 * Using internal LED
 * Set status (send "on" , "1", "yes", "enabled", "true" to turn on, anything else to turn off) on /demo-device/light/cmd
 * Get the acknowledge on /demo-device/light/ack
 */

#include <basex.h>

BoolGPOut mySwitch(BUILTIN_LED, "light/cmd", "light/ack");

void setup() {
  LoopManager::instance().setup("device", "wifi_name", "wifi_pass", "mqtt_broker_ip",
                                /*mqttUser=*/ NULL,/*mqttPass=*/ NULL);
}

void loop() {
  LoopManager::instance().loop();
}
