# What is BaseX: #
BaseX is an arduino package that allows you to program slave-devices (such as ESP8266) to be controlled from a central MQTT server.

## How to install
I'm not quite sure... You should have a directory of your Arduino libraries somewhere, and you should copy the code into there
I will try to find a way to publish this package perhaps, but until then...
- Depends in package:
- pubsubclient: https://pubsubclient.knolleary.net/
- ESP8266: ESP8266WiFi https://github.com/esp8266/ESPWebServer/blob/master/library.properties
- ArduinoJson https://arduinojson.org/

## How to use ##
Programming is done in C++, but so much of the annoying common functionality is pre-baked inside, that you often are left with ~5 lines of code for standard applications.

### How exactly to use ###
The idea behind BaseX, is that you have ready-made modules (Button, GPIO, ...) which trigger events (called Signals), and react to events (called Slots).   

**In most cases, what you would need to do is:**  
* At the beginning of code, Declare objects you want to use.  
* At the `setup()` function, call `LoopManager::instance().setup(...)`, for configuring Wifi & MQTT settings, possibly assigning a LED for showing activity.  
* Still at `setup()` you can define which Signals are connected to what Slots.   
* At the `loop()` command, call the `LoopManager::instance().loop()` function.  

## A Simple Example, please ##

### MQTT Light Switch ###
```
#include <basex.h>

BoolGPOut mySwitch(D5, "light/cmd", "light/ack");

void setup() {
  LoopManager::instance().setup("device", "wifi_name", "wifi_pass", "mqtt_broker_ip");
}

void loop() {
  LoopManager::instance().loop();
}
```

**Explanation about the code:**  
Let's explain is from bottom to top:  
* In the `loop()` function, all you do is call the `LoopManager::instance().loop();`, to make the system work.  
* In the `setup()` function, the call to `LoopManager::instance().setup(...)` actually tells the system about the "device-name" (which has to be unique per device/broker), and WiFi/MQTT configs and credentials.  
* That means that as soon as the device connects the MQTT broker, it will send a [retained] message "In busyness" to the topic `device/status`, and try to sync all relevant data from all relevant topics. A *will* message saying `gone` is pending on `device/status` as well.  
* That also means that all device's communication will be under `device/` topic in that MQTT broker.  
* You can also specify which GPIO to use for "Status LED", that has some special blink codes to show what's going on, or set it to `Communication::NO_LED` (=-1) to disable. Default is BUILTIN_LED  
* Now look at the BoolGPOut decleration. It means we want to have a Boolean GPIO used for Output. Pin is D5. Next two strings indicate the "command" and "ack" topics, which are used to set the value and get feedback from the device. Yes. You can disable it.  


### Complicating the example: MQTT Light Switch with a Button ###
So far so good, but we can also attach a button to the device, and control on the light without having to search for the phone... 

Let's add a button. Just next to the `GPOut` declaration, we put:
```
MAKE_BUTTON(myButton, D4);
```

This will create a variable called `myButton` (couldn't make it without the macro... sorry), which is connected to GPIO D4.  
The [push] button is expected to connect GND to pin D4 when pressed. No other electricity required.  

To make the button toggle the light, we need to connect the `PRESSED` Signal of the button to the `INCREMENT` Slot of the GPOut. Do this inside the `setup()` function:  

```
LoopManager::instance().connect(myButton, Signal_t::PRESSED, mySwitch, Slot_t::INCREMENT);
```

The `INCREMENT` Slot causes the boolean GPOut to toggle state, so whenever a `PRESSED` Signal comes out of the button, it will change the switch status.  
A change in the switch status automatically updates the broker on the `device/light/ack` topic.  

### And that's about it? ###

**No!**   
Actually, the `Button` object supports some more options: `CLICK_ONLY, DOUBLE_CLICK, PRESSED, RELEASED, HOLD, HOLD_END`
(`CLICK_ONLY` is fired only when we're sure no `DOUBLE_CLICK` can come, so it's a bit delayed)  
Furtheremore, the `Button` object can publish all its events in it's "ack" topic, so you can decide whatever you want on the server (e.g. Home Assistant)  
So this single button can allow you to turn off other devices, if you, for example, long-click (`HOLD`) it.  

To make the button emit events on the MQTT, just write the `ack` topic in the same fashion the switch does (no `cmd` topic for a button).

See below detailed explanation about the different `Object`s you can use in the application.  

### Okay, I'll read. I promise. Is that about it?
Well... Still not really.  
Firstly, there's a bad nasty paragraph about the `Object` class itself, which is the most basic entity in the system, and from which
you can quite easily develop new gizmos.  
Then there's the thing of the `LoopManager` class, which let you manage the application.  
Finally, there's the `DeviceConfig` and `Communication` classes, which I let you read the code about it, 
because they're not directly exposed to the user.

Here's the full `LoopManager::setup(...)` function parameters:  

``` 
void setup(const char *deviceName=NULL,
           const char *wifiName=NULL,
           const char *wifiPass=NULL,  
           const char *mqttHostname=NULL,
           const char *mqttUser=NULL,
           const char *mqttPass=NULL,  
           int ledPin=BUILTIN_LED,
           int baudRate=115200,
           const char *willMessage ="gone",
           const char *deviceStatusTopic="status")   
```

`deviceName` - A unique, friendly name for your device. Will serve as the base "path" for all MQTT activity.  
`wifiName` - Name of your WiFi network. If you have several routers at home with the same net, I think it chooses the strongest one.  
`wifiPass` - That sensitive secret information... Have to have it for the compiler.. Sorry.  
`mqttHostname` - Usually the IP of the broker, like 192.168.0.100 or so...  
`mqttUser` - If you use credentials for the MQTT, here it goes.  
`mqttPass` - If you use credentials for the MQTT, here it goes too.  
`ledPin` - Defaults to BUILTIN_LED. Any Arduino named pin. See the behavior of the LED below. See `BlinkyLed` 
to understand the code names  
`baudRate` - For the Serial. So far no way to disable Serial.
`willMessage` - Text to send as will message, NULL to not send anything.
`deviceStatusTopic` - The topic on which device status ("In busyness", "gone", and network settings) are published

You can keep all communication fields as `NULL`. In this case, the application will work without anything from the outer world.  


# GPOut
##Handles outputing signals using GPIO
###Usage
Declaration (before first function):  

```
BoolGPOut (int pin, const char *cmdTopic, const char *ackTopic, bool  defaultValue=false, bool inverted=true, bool resetOnCommLoss=true)
IntGPOut  (int pin, const char *cmdTopic, const char *ackTopic, int   defaultValue=    0, bool inverted=true, bool resetOnCommLoss=true)  
FloatGPOut(int pin, const char *cmdTopic, const char *ackTopic, float defaultValue=    0, bool inverted=true, bool resetOnCommLoss=true)   
```
*NOTE: I don't remember testing the `int` and `float` flavors. Help is much appreciated.*
####Params:  

`int pin` - An Arduino pin number  
`const char *cmdTopic` - String to concatenate to device name, and listen to commands in.   
`const char *ackTopic` - String to concatenate to device name, and send all events onto. 
`bool  defaultValue` - Initial (and after communication loss) value  
`bool inverted=true` - Invert the output signal. In `int`/`float` 0 will have full duty cycle.  
`bool resetOnCommLoss=true` - Weather or not to act when a `LONG_NO_SEE` signal is received.

To MQTT-control the GPIO, you only need to declare this object.  
Floating point `FloatGPOut` expect the inputs to be in range `0-1`, while `IntGPOut` expect `0-1023`    
  
###Signals:
Everything a `Variable` supports

###Slots:
Everything a `Variable` supports





# Button
###Handles a standard pushbutton
Declaration (before first function):  
`````MAKE_BUTTON(name, PIN, ackTopic)`````
####Params:  

`name` - Variable name to create  
`PIN` - An Arduino pin number, like D3, ... (I had some problems with the mapping on my system. Not sure why)
`ackTopic` - String to concatenate to device name, and send all events onto.  
  
Calling this macro creates the `Button` object, declares an ISR function for it to handle, and sets everything for you.
When the button is pressed, the ISR function is called, and on the next `loop()` iteration, the `Button` will check the timings 
of the last button changes.  
Note: cheap buttons tend to create many many interrupt calls for every single click, because they have bad contacts.    
The `Button` object filters redundant changes in the GPIO, and decides when the button was pressed or released, to fire matching Signals.    
If the button is being pressed for long enough, it will also send a `HOLD` event. In such cases, when released, in addition to the `RELEASED` events, it will also send `HOLD_END`.    
If repetitive clicks are detected (and they are fast enough), Then in addition to the `PRESSED` and `RELEASED`, a `DOUBLE_CLICK` event will be fired.  
In cases of `PRESS` -> `RELEASE` events, after the option of a `DOUBLE_CLICK` is rejected (too much time passed), an event called `CLICK_ONLY` will be fired.  
    
###Some other stuff about buttons
`inverted` - Means the button is grounding the GPIO pin (default). You can set that with `void setPinMode(int modeOfPin=INPUT_PULLUP, bool inverted=true)`  
`quiet` mode: You can toggle the button sending all events on MQTT using `void beQuiet(bool quiet=true)`  
`bool isPressed()` - You can directly read the status of the `Button` using this. `inverted` applies here too.
    
  
###Signals:
`CLICK_ONLY`, `DOUBLE_CLICK`, `PRESSED`, `RELEASED`, `HOLD`, `HOLD_END`    

###Slots:
None. A button only transmits things.  
    
    
    
    
# Variable
###Store internal state, hold remotely-controlled numbers
A varialbe is used to store an internal state (possibly reporting via MQTT), as a connector between other objects, or as any remote-controlled 
data to be used (Like interval between pulses, timer value, ...)

Declaration (before first function):    
```BoolVar  (bool defaultValue, const char *cmdTopic=NULL, const char *ackTopic=NULL, T *pWhere=NULL, bool resetOnCommLoss=true)```  
```Uint8Var (uint8 defaultValue, const char *cmdTopic=NULL, const char *ackTopic=NULL, T *pWhere=NULL, bool resetOnCommLoss=true)```  
```IntVar   (int defaultValue, const char *cmdTopic=NULL, const char *ackTopic=NULL, T *pWhere=NULL, bool resetOnCommLoss=true)```  
```FloatVar (float defaultValue, const char *cmdTopic=NULL, const char *ackTopic=NULL, T *pWhere=NULL, bool resetOnCommLoss=true)```  

####Params: (with `T` being one of the types)  
`T defaultValue` - Default value for init and after reset/long-no-see.  
`const char *cmdTopic` - Topic to subscribe to to get updates, or `NULL` to skip.   
`const char *ackTopic` - Topic to publish value on change, or `NULL` to skip.  
`T *pWhere=NULL` - Make the variable use an external location, e.g. LED's brightness value. In such cases, the variable does not track external changes of the value.
`bool resetOnCommLoss` - Set to `true`if you want the `Variabl` to reset itself.  
    
Use this to hold any remotely-controlled value. It is also possible to use a variable as an internal state holder, such as something 
that does an `INCREMENT` on every `Button` `PRESSED`. 

The `Variable` can emit the Signal `CHANGED`, and by default reacts to `SET`, `PUBLISH`, `INCREMENT`, `DECREMENT`, `RESET`, `LONG_NO_SEE` (which does `RESET`)  

###Some other stuff about Variables
- The `BoolVar` parses the text input from the `cmd` topic, and will set to `true` iff gets any of  "on" , "1", "yes", "enabled", "true". Not case sensitive.
- `void setLimits(T minValue, T maxValue, int limitType=BOUNDED)` can set the `Variable`'s min and max values, as well as the limit type.
- `limitType` can be any of: `UNLIMITED`, `BOUNDED`, `WRAP_AROUND`. It will affect any attempt to set the variable's value.  
- `UNLIMITED` means any value you sent will be cast to the variable's ability (e.g. 0-255 for `Uint8`).    
- `BOUNDED` means the value you try to set will be saturated at the `minValue`, `maxValue`.  
- `WRAP_AROUND` means that if you exceed the limits given, the value will have a "modulo" value by the limits.  
- *Note*: `maxValue` is the first-not-allowed-value. This means that if you want an `IntVar` to be in the range 0-5, you need to pass `0, 6`.
- `std::string toString()` : Every `Variable` can represents itself as a string, so you can `Serial.print()` it.
- `value()`, `setValue(...)`: To get/set the `Variable`'s value from your program, please use these.  

*NOTE*: Not sure how much float/int variables are tested. I had some problems with the `WRAP_AROUND`. Help will be much appreciated.  



# BlinkyLed 
###Do neat things with LEDs

The BlinklyLED aims to handle LEDs blinking and other effects.  
You can quickly make a LED, blink, fade and stuff. See all modes below.

First, any `BlinkyLed` needs a `LedConfig` struct, to tell it what to do. The config must remain intact during operation time.
Any change in the config will immediately apply to the BlinkyLed.  
This means that if you have several LEDs that share a configuration (and why would you? Oh, you might) you can use a single config object.


Declaration (before first function):      
`LedConfig(uint8 style=BLINK, long period=1000, bool invert=true, long onTime=400, long fallTime=200, float gamma=3)`

####LedConfig Params:  
**`style`**: Any of the following. Probably easiest is to look at the charts showing the behavior.  
- `LedConfig::OFF` - Led is off.    
- `LedConfig::ON` - Led is on. Adjustable brightness.    
- `LedConfig::BLINK` - Led blinks once per `period`, for `onTime` milliseconds.   
- `LedConfig::BLINK2` - Like `BLINK`, but the middle 1/3 of the pulse is off (=a double-blink).   
- `LedConfig::DECAY` - Led is linearily decaying to zero from every change point during `period`. Use `USER_1` slot or `bump()` to bump it.   
- `LedConfig::TRIG` - Led is "breathing" every `period`, "triangle's decend" takes `fallTime`, whole pulse ends after `onTime` milliseconds.    
- `LedConfig::TRANS` - Led is performing a linear transient every brightness change command, transient lasts for `period` milliseconds.    

`period` - Number of milliseconds for each period (`BLINK`, `BLINK2`, `DECAY`, `TRIG`, `TRANS`).  
`invert` - The LED is inverted, like ESP8266's built in LED.    
`onTime` - See `style` above.  
`fallTime` - See `style` above.  
`gamma` - Apply a gamma function. I guess every LED has different properties regarding that. `3` seems nice on the builtint LED (Wemos D1 Mini)
  
*Notes:*
- To find best brightness / gamma, use a `TRIG` LED, with `fallTime = 0.5 * period`, then play with the value until you think the brightness increases steadily.   
- The `targetBrightness` sets maximum LED's power in all modes except for `DECAY`, in which `targetBrightness` will set the *decay-to* value, so set it to `0`  



####BlinkyLed Declaration:  
`BlinkyLed(uint8 pin, LedConfig &ledConfig, int targetBrightness=LedConfig::MAX_VAL)`

(Note: `static const int MAX_VAL = 255`)

####BlinkyLed Params:
`pin` - An Arduino pin number. See `pins.h`   
`ledConfig` - The aforementioned thingybob.  
`targetBrightness` - An integer in range `0-MAX_VAL` to tell how bright you want the LED to be. 
 
#### Using `BlinkyLed` with events
- By default, `BlinkyLed` changes `targetBrightness` on `INCREMENT` and `DECREMENT`. You can also use `SET` to change brightness
- If the mode is `DECAY`, then `INCREMENT` triggers the `bump()` method, which increases the brightness by some amount, then decays.  
This is the mode that `LoopManager` uses for the status LED when connected. Every activity "bumps" the LED, and it decreases again to zero.


#### Using `BlinkyLed` for RGB effect strip
*Disclaimer:* Not yet tested myself. Help much appreciated here.  
You can define 3 `BlinkyLed` objects on 3 pins. Let them share the `LedConfig`, set it to `LedConfig::TRANS` mode, 
but send different `targetBrightness` for the LEDs.
To get the "Jet" colormap used by Matlab/matplotlib, you can try to send these values:  
(0, 0, 512) -> (0, 0, 1023) -> (0, 512, 1023) -> (0, 1023, 1023) -> (512, 1023, 512) -> (1023, 1023, 0) -> 
(1023, 512, 0) -> (1023, 0, 0) -> (512, 0, 0)  




# Object
This is the basic object from which all other inherit.  
The purpose of the `Object` class is to provide something that is recognized by the system (called `LoopManager`), it can execute its own 
code, and use Signals and Slots

If you want anything that is custom made, probably the easiest way to accomplish would be to define a class 
of your own, which inherits from `Object`.  

Have a look at the complicated-button-and-led example for the details, but I'll repeat here.  

Here's an example of catching the `INCREMENT` and `DECREMENT` Slots to change the style of a LED.  
(The advanced readers already know it can be accomplished with an IntVar that points to the LedConfig.style)  

 ```
LedConfig lc(LedConfig::BLINK2);

class Stam : public Object {
  void increment(Object *pOther) {
    lc.style = (uint8) i.value() % LedConfig::NUM_STYLES;
  }
} stam;

```

Now, once you connect the `stam` object to anything, it just works :-)  

`LoopManager::instance().connect(myButton, Signal_t::CLICK_ONLY, stam, Slot_t::INCREMENT);`

###The `Object` class has the following methods to override:

`virtual const char *type() const {return "Some representitive name";}`
Return a printable name. You will see this name in the Serial monitor, when the object is triggered

`virtual void onMessage(const char *topic, const byte *payload, long length);`
Handle an incoming MQTT message

`virtual void loop()`
Any logic to perform if your object has a state. Will be called once per "Arduino `loop()`" function.  

`virtual void setup();`
Will be called in "Arduino's `setup()`" once. Mostly related to DeviceConfig

`virtual void onConnect(Object *other=NULL)`
Will be called when MQTT connects. Expect NULL on the `other`.   

`virtual void onLongDisconnect(Object *other=NULL) {}`
Will be called when MQTT connection is lost for 2 minutes. Expect NULL on the `other`.  

`virtual void publish(Object *other=NULL)`
Called upon `Slot_t::PUBLISH`. Expect NULL on the `other`. 
           
`virtual void setFrom(Object *other=NULL)`
Called upon `Slot_t::SET`. You can call other->value() to get the value. Make sure data types match.  
 
`virtual void longTimeNoSeen(Object *other=NULL)`
Called upon `Slot_t::SET`. You can call other->value() to get the value. Make sure data types match.  
    
`virtual void increment(Object *other=NULL)`
Called upon `Slot_t::INCREMENT`

`virtual void decrement(Object *other=NULL)`
Called upon `Slot_t::INCREMENT`

`virtual void reset(Object *other=NULL)`
Called upon `Slot_t::RESET`
             
`virtual void user_1(Object *other=NULL)`
Called upon `Slot_t::USER_1`
            
`virtual void user_2(Object *other=NULL)`
Called upon `Slot_t::USER_2`
            
`virtual void user_3(Object *other=NULL)`
Called upon `Slot_t::USER_3`
            