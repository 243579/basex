#include "basex.h"
#define DHT 3
#include <DHT.h>

/*******************************
 * A Skeleton for my CPP "drivers" of certain hardware & software
 * This is an example file that has an MQTT-controlled GPIO (lamp), a button (but1), status led and DHT, reporting every 2 minutes
 * It also has a counter (which you can modify via MQTT) that counts how many times you toggled the light since boot.
 * If you long-click the button, the light goes off
 * If you double-click the button, the counter is reset
 * If you click the button (with a small delay), the light is toggled
 * In addition, all button's activity is sent over MQTT, so you can do whatever in the server-side
 *
 * Some old functionality to restore - - - - -
 * LED has several modes, between which you toggle with a button click
 * Intensity is changed when holding the button
 * A double-click turns the LED on/off
 * You should:
 * Inherit and instanciate any of the classes you find in the h files around. Example:
 * * * Inherit Button as MyButton, override "pressed()", and use _isDblClick to determine what you want to do
 * This sample code handles a button, the builtin LED, and communicates with the MQTT broker
*/


#define DHT_PIN D6
#define LAMP_PIN D5
#define BUTTON_PIN D4
#define LED_PIN BUILTIN_LED

GPOut<bool> lamp(LAMP_PIN, "lamp/cmd", "lamp/ack");
Button<BUTTON_PIN> button("button/ack");

IntVar counter( /* defaultValue */ 0, 
                /* cmdTopic     */ "counter/cmd",
                /* ackTopic     */ "counter/ack");

FloatVar tempC(std::numeric_limits<float>::quiet_NaN(), NULL, "temp/ack");
DHT dht(DHT_PIN, DHT22);

// This allows us to manipulate the lamp from the button in user-defined ways:
class MyStuff : public Object {
  using Object::Object;

  virtual void user_1() {
    // Makes a long-click to turn off the light
    lamp.setValue(false); // false = off
  }

  virtual void user_2() {
    counter.setValue(0); // Reset the counter
  }
};

MyStuff myStuff;

void setup() {
  LoopManager::instance().setup("demo-device", "wifi-name", "password", "192.168.20.222");
  LoopManager::instance().connect(lamp, Signal_t::CHANGED, counter, Slot_t::INCREMENT); // Count every change in lamp state
  LoopManager::instance().connect(button, Signal_t::CLICK_ONLY, lamp, Slot_t::INCREMENT); // Toggle lamp on every click (but not double-click)
  LoopManager::instance().connect(button, Signal_t::HOLD, myStuff, Slot_t::USER_1); // Holding button turns off the light
  LoopManager::instance().connect(button, Signal_t::DOUBLE_CLICK, myStuff, Slot_t::USER_2); // Double-clicking resets coutner
}

Repeater sendTemp(120000); // Will trigger DHT to send report every 2 minutes

void loop() {
  LoopManager::instance().loop();

  if (sendTemp.loop() && ~isnan(tempC.value())) {
    tempC.setValue(dht.readTemperature());
  }
  
}















// #define LED_MODE_CMD_TOPIC "led/cmd/mode"
// #define LED_MODE_ACK_TOPIC "led/ack/mode"

// #define LED_BRIGHTNESS_CMD_TOPIC "led/cmd/brightness"
// #define LED_BRIGHTNESS_ACK_TOPIC "led/ack/brightness"

// #define BUTTON_ACK_TOPIC "button/ack"

// const uint8_t BUTTON_1 = 0;
// const uint8_t RELAY_1 = D6;
// const uint8_t STATUS_LED = 2;



// // LedConfig ledConfig(LedConfig::ON);
// // BlinkyLed led(STATUS_LED, ledConfig);
// // Uint8Var ledMode(/* defaultValue */ LedConfig::ON,
// //                  /* cmdTopic     */ LED_BRIGHTNESS_CMD_TOPIC,
// //                  /* ackTopic     */ LED_BRIGHTNESS_ACK_TOPIC,
// //                  /* minValue     */ LedConfig::OFF,
// //                  /* maxValue     */ LedConfig::NUM_STYLES - 1,
// //                  /* pWhere       */ &ledConfig.style);


// // int lastLedMode=0;

// // void sendButtonStatusReport(const char *what = NULL) {
// //   if (what == NULL) {
// //     what = "what??";
// //   }
// //   comm.publish(BUTTON_ACK_TOPIC, what, false);
// // }

// // class MyButton : public Button {
// //   using Button::Button;
// //   long _lastUpdate;
// //   static const long INC_INTERVAL = 30; // mSec

// //   virtual void released() {
// //     // The logic is on the released, so we don't think a hold is a click
// //     if (_isDblClick & !_inHold) {
// //       // Set to full brightness, but first undo the first click of the double
// //       ledMode.decrement();
// //       Serial.print("Double click - full power");
// //       led.setBrightness(LedConfig::MAX_VAL);
// //       sendButtonStatusReport("double");
// //     } else {
// //       if (_inHold) {
// //         Serial.println("Button released (hold). Resuming previous mode.");
// //         ledMode = lastLedMode;
// //       } else {
// //         // Single click release
// //         ledMode.increment();
// //         Serial.print("Click - next mode. Now ");
// //         Serial.println(ledConfig.style);
// //         sendButtonStatusReport("click");
// //       }
// //     }
// //     acknowledgeState();
// //   }

// //   virtual void hold() {
// //     sendButtonStatusReport("hold");
// //     Serial.println("Hold");
      
// //     ledMode = LedConfig::ON;
// //     _lastUpdate = millis(); // Start changing brightness. Will actually be done in the "aux" function.
// //   }

// //   virtual void aux() {
// //     if (_inHold) {
// //       long now = millis();
// //       if (now - _lastUpdate >= INC_INTERVAL) {
// //         _lastUpdate = now;
// //         led.setBrightness((led.brightness() + 10) % 1024);
// //         Serial.print("Setting brighness to ");
// //         Serial.println(led.targetBrightness);
// //       }
// //     }
// //   }
// // };

// // MyButton but1(BUTTON_1, true);
// // ICACHE_RAM_ATTR void isr1() {
// //   but1.isr();
// // }

// // Repeater myRepeater(2000); 

// // void acknowledgeState() {
// //   Serial.print("Updating status: Mode = ");
// //   Serial.print(ledMode.value());
// //   Serial.print(", brightness = ");
// //   Serial.println(led.brightness());

// //   char buf[10];
// //   snprintf(buf, sizeof(buf), "%d", ledConfig.style);
// //   comm.publish(LED_MODE_ACK_TOPIC, buf);
// //   snprintf(buf, sizeof(buf), "%d", led.brightness);
// //   comm.publish(LED_BRIGHTNESS_ACK_TOPIC, buf);
// // }

// // void setup() {
// //   // put your setup code here, to run once:
// //   comm.setup();
// //   acknowledgeState();  // Set relay off (=high), Chip boots with LOW on all GPIOs
// //   attachInterrupt(digitalPinToInterrupt(but1.pin()), isr1, CHANGE);
// //   lastLedMode = ledConfig.style;
// //   Serial.println("Setup finished");
// // }

// // void loop() {
// //   LoopManager::instance().loop()
// //   if ((ledConfig.style == LedConfig::DECAY) && myRepeater.loop()) {
// //     led.bump(1000); // A pulse every 2 seconds
// //     Serial.println("BUMP!");
// //   }
// //   if (LoopManager::instance().canReadRt()) {
// //     Serial.print("Average RT ");
// //     Serial.print(comm.rtMeasure() * 100);
// //     Serial.println("%");
// //   }
// //   comm.loopEnd(10); // A short delay to let ISRs run...
// // }
