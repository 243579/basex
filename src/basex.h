/*
 * This file if meant to let the user a single include to rule them all
 * If you want, you can include directly any of the needed files in the packacge
 */
#ifndef __BASEX_H__
#define __BASEX_H__

#include "pins.h"
#include "blinkyLed.h"
#include "rgbLed.h"
#include "button.h"
#include "communication.h"
#include "gpout.h"
#include "loopManager.h"
#include "object.h"
#include "repeater.h"
#include "variable.h"

#endif // __BASEX_H__




