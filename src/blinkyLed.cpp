#include "blinkyLed.h"
#include "variable.h"

const char *LedConfig::names[] = {"OFF", "ON", "BLINK", "BLINK2", "DECAY", "TRIG", "TRANS", "ERROR"};
//                                 0      1     2        3         4        5       6

LedConfig::LedConfig(uint8 style, long period, bool invert,
                     long onTime, long fallTime, float gamma) :
  period(period),
  style(style),
  invert(invert),    
  onTime(onTime),
  fallTime(fallTime),
  gamma(gamma)
{
}

BlinkyLed::BlinkyLed(uint8 pin, LedConfig &ledConfig, int targetBrightnessIn):
  Object(NULL, NULL),
  _pin(pin),
  _ledConfig(ledConfig) 
{
  pinMode(_pin, OUTPUT);
  setBrightness(targetBrightnessIn);
  currValue = targetBrightness;
  initialValue = currValue; // Value at the beginning of the TRANS action
  lastPeriodStart = millis();
}

void BlinkyLed::loop() {
  long now = millis();
  long timePassed = millis() - lastPeriodStart;
  float fVal=0.5; // in range 0-1, where 1 represent target brightness. Init for happy compilers only
  float valOff = 0;
  float valOn = (float)targetBrightness / LedConfig::MAX_VAL;
  switch (_ledConfig.style) {
  case LedConfig::OFF:
    fVal = valOff;
    break;
  case LedConfig::ON:
    fVal = valOn;
    break;      
  case LedConfig::BLINK2:      
  case LedConfig::BLINK:
  case LedConfig::TRIG:
    if (timePassed > _ledConfig.period) {
      lastPeriodStart += _ledConfig.period;
      timePassed -= _ledConfig.period;
    }
    if ((_ledConfig.style == LedConfig::BLINK2) &&
        (timePassed <= _ledConfig.onTime)) {
      timePassed = (3 * timePassed) % (2 * _ledConfig.onTime);
    }             
    if (timePassed > _ledConfig.onTime) {
      fVal = valOff;
    } else {    
      if (_ledConfig.style == LedConfig::BLINK || _ledConfig.style == LedConfig::BLINK2) {
        fVal = valOn;
      } else if (_ledConfig.style == LedConfig::TRIG) {
        int peakTime = _ledConfig.onTime - _ledConfig.fallTime;
        int relativeTime = timePassed - peakTime;
        float denom = (float) (relativeTime > 0 ? _ledConfig.fallTime : -peakTime);
        float belowPeak = relativeTime / denom;
        fVal = (1.0 - belowPeak) * valOn;
      }
    }
    break;
  case LedConfig::DECAY:
  case LedConfig::TRANS:
    // In DECAY, we want effective targetBrightness to be 0
    float usedBrightness = _ledConfig.style == LedConfig::DECAY ? 0 : targetBrightness;
    if (usedBrightness != lastTargetBrightness) {
      // We had a change in the brightness. Set lastPeriodStart to now
      // This supports a IntVariable which is pointTo'ing our targetBrightness / style / ...
      lastPeriodStart = millis();
      timePassed = 0;
      lastTargetBrightness = usedBrightness;
    }
    if (timePassed > _ledConfig.period) {
      // Long time no change
      lastPeriodStart = now;
      initialValue = lastTargetBrightness = usedBrightness;
      timePassed = 0;
    }
    float wNext = timePassed / (float) _ledConfig.period;
    float wPrev = 1 - wNext;
    if (initialValue != usedBrightness) {
      // Calculate the transient
      fVal = (wPrev * initialValue + wNext * usedBrightness) / LedConfig::MAX_VAL;
    } else {
      // Avoid annoying numeric errors
      fVal = valOn;
    }
    break;
  }
  setFloat(fVal);
}

void BlinkyLed::setFrom(Object *other) {
  // Expecting an IntVar in range [0, LedConfig::MAX_VAL] (=1023) for targetBrightness
  IntVar *pIntVar = (IntVar *)other;
  int newBrightness = pIntVar->value();
  setBrightness(newBrightness);
}

void BlinkyLed::increment(Object *other) {
  // Bump the LED if we're in decay mode, increase brightness if not
  if (_ledConfig.style == LedConfig::DECAY) {
    bump();
  } else {
    setBrightness(targetBrightness + 4);
  }
}

void BlinkyLed::decrement(Object *other) {
  // Ignore if we're in decay mode, decrease brightness if not
  if (_ledConfig.style == LedConfig::DECAY) {
  } else {
    setBrightness(targetBrightness - 4);
  }
}

void BlinkyLed::setFloat(float fVal) {
  // Set the value from floating-point value, 0=Off, 1=target brightness
  // Also apply Gamma correction
  currValue = CLIP(fVal, 0, 1) * LedConfig::MAX_VAL;
  float rel_val = currValue / (float) LedConfig::MAX_VAL;
  float gamma = _ledConfig.gamma;
  float gVal = (float) pow(rel_val, gamma);
  int tempVal = (int)((float) gVal * LedConfig::MAX_VAL);
  int val2send = _ledConfig.invert ? LedConfig::MAX_VAL - tempVal : tempVal;
  analogWrite(_pin, val2send);
}

void BlinkyLed::bump(int delta) {
  // For DECAY mode: Bump the power, and let it decay on
  initialValue = currValue + delta;
  initialValue = CLIP(initialValue, 0, LedConfig::MAX_VAL);
  // targetBrightness = 0; // Decay to 0
  lastTargetBrightness = -1; // Trigger TRANS to calculate its things
  lastPeriodStart = millis(); // As long as DECAY mode has a different case
}

void BlinkyLed::setBrightness(int newBrightness) {
  lastTargetBrightness = targetBrightness;  // So loop can detect brightness was changed in TRANS mode
  targetBrightness = CLIP(newBrightness, 0, LedConfig::MAX_VAL);
}
