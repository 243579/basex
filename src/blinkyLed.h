#ifndef __BLINKYLED_H__
#define __BLINKYLED_H__

#include <Arduino.h>
#include "object.h"

struct LedConfig {
  enum { OFF    , // 0
         ON     , // 1
         BLINK  , // 2: On for onTime, off for rest of period
         BLINK2 , // 3: On for 2 pulses of 1/3 onTime, off for rest of period
         DECAY  , // 4: After each bump() call, decay to zero
         TRIG   , // 5: Increase then decrease. Shut down from onTime to period. Decreasing takes fallTime
         TRANS  , // 6: Each setBrightness will result a linear transient from current state, that takes period
         NUM_STYLES } LedStyle;
  static const char *names[];
  uint8 style; // Any of the enum above (not NUM_STYLES though)
  bool invert; // Means LED is on in LOW, and 0, and off in HIGH (Like Built-in LED)
  long period; // mSec of the total period, in LED_TRANS - transition time
  long onTime; // mSec, for LED_BLINK, LED_TRIG and their *2 versions
  long fallTime; // mSec, for LED_TRIG ( Try onTime/2), for LED_DECAY - how fast to turn off from full power
  float gamma; // Gamma function to make it more linear. Probably per-led-type, 3 seems pretty fine for builtin LED (D1 mini)
  static const int MAX_VAL = 255;
  LedConfig(uint8 style=BLINK, long period=1000, bool invert=true,
            long onTime=400, long fallTime=200, float gamma=3);
};

class BlinkyLed : public Object {
private:
  uint8 _pin;
  LedConfig &_ledConfig;
  int lastTargetBrightness; // To detect target brightness changes in TRANS mode. Note we might skip setBrightness if we a variable pointTo to our brighness (as recommended)
  void setFloat(float fVal);
public:
  long lastPeriodStart; // Either the epoch in BLINK / TRIG, or the last bump in DECAY
  int targetBrightness; // In 0-1023 range. 0=Off. Like a "master volume"
  long currValue;
  int initialValue; // For DECAY, what value we started at, from last bump

public:
  BlinkyLed(uint8 pin, LedConfig &ledConfig, int targetBrightness=LedConfig::MAX_VAL);
  virtual const char *type() const {return "BlinkyLed";}
  virtual void loop();
  virtual void setFrom(Object *other=NULL);
  virtual void increment(Object *other=NULL);
  virtual void decrement(Object *other=NULL);

  void bump(int delta=LedConfig::MAX_VAL / 2);
  void setBrightness(int newBrightness);

};

#endif // __BLINKYLED_H__
