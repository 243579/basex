// A class to represent a button. Has: Press, Release, Click, dblClick, hold, holdEnd, clickOnly
// To use:
// 1. Inherit into a class of yours, and override any method you like to use
// 2. Call button.loop in your loop
//
// To tweak:
// STABILIZE_TIME - The time passing from first electrical contact until signal stabilizes
// LONG_CLICK_THRESHOLD - mSec to wait until we decide it's a HOLD action
// DOUBLE_CLICK_THRESHOLD - Time from press-1 to press-2 to consider as a double-click


#ifndef __BUTTON_H__
#define __BUTTON_H__

#include "object.h"
#include "communication.h"

// See pins.h for explanation about pin names, and if they work with ISRs
template <uint8_t PIN>
class Button: public Object {
private:

  // Button changes are recorded in an interrupt
  static const int BUF_SIZE = 128; // Size of interrupt buffer recording.
  static const long STABILIZE_TIME = 10000; // mSec before we decide we're clicked
  static const long LONG_CLICK_THRESHOLD = 750000; // mSec
  static const long DOUBLE_CLICK_THRESHOLD = 300000; // mSec
  static const long IDLE_TIME = 100000000; // To prevent wrap-around problems

  static long _timing[BUF_SIZE];
  static bool _isHigh[BUF_SIZE];
  static char _nextWrite;
  static char _nextRead;
  static bool _inverted;
  static bool _inHold;  // Turns on when we detect clicked for long enough
  static bool _isDblClick; // Sets after a double-click, and cleared after the call to press
  static bool _hadFirstClick;
  static char _status;  // Currently believed state of the button
  static long _lastEventTime;  // Last update of _status
  static long _pressTime1; // To detect a long-click
  static long _pressTime2; // To detect a double-click
  static bool _isPressed;  // For accessor
  static bool _publish;    // If true, publish every event you have
  void (*_isr)();

public:

  Button(void (*xisr)(), const char *ackTopic=NULL) :
    Object(NULL, ackTopic)
  {
    _nextWrite = 0;
    _nextRead = 0;
    _inverted = true;
    _inHold = false;
    _isDblClick = false;
    _hadFirstClick = false;
    _isPressed = false; // Will be updated in loop
    _status = digitalRead(PIN) == HIGH;
    _pressTime1 = _pressTime2 = _lastEventTime = micros() - IDLE_TIME;
    _publish = true;
    pinMode(PIN, INPUT_PULLUP);
    _isr = xisr;
  }

  void setISR(void (*xisr)()) {
    // Probably needed on ESP8266, but not on Arduino, because one does not simply put a method in ICACHE_RAM_ATTR
    _isr = xisr;
  }

  void setPinMode(int modeOfPin=INPUT_PULLUP, bool inverted=true) {
    // Mostly pins are INPUT_PULLUP, and the click grounds the pin, so inverted is true
    pinMode(PIN, modeOfPin);
    _inverted = inverted;
  }

  void beQuiet(bool quiet=true) {
    // Disable sending on MQTT all events
    _publish = !quiet;
  }

  virtual const char *type() const {return "Button";}

  static void isr() {
    Button::_timing[Button::_nextWrite] = micros();
    Button::_isHigh[Button::_nextWrite] = digitalRead(PIN) == HIGH;
    Button::_nextWrite = (Button::_nextWrite + 1) % BUF_SIZE;
  }

  virtual void setup() {
    Object::setup();
    attachInterrupt(digitalPinToInterrupt(PIN), _isr, CHANGE);
  }

  bool isPressed() { return _isPressed; }

  void loop() {

    // Can be that we pressed the button, and quickly (< STABILIZE_TIME) depressed after.
    // So in the next loop we'll see zero events, but state is different
    // We mock an event of current pin state with current timing.
    bool once = _nextRead == _nextWrite;
    char nextIsHigh = (once) ? digitalRead(PIN) == HIGH : _status;
    long nextTiming = micros();
    char nextStatus = nextIsHigh;

    // Outer loop because we might have more than 1 event in the same main loop call
    while (once || ((int)_nextRead) != ((int)_nextWrite)) {
      once = false;
      // Inner loop consumes all events until stablized
      while (((int)_nextRead) != ((int)_nextWrite)) {
        long nextTiming = _timing[_nextRead];
        char nextIsHigh = _isHigh[_nextRead];
        
        _nextRead = (_nextRead + 1) % BUF_SIZE;
        if (nextTiming - _lastEventTime < STABILIZE_TIME) {
          // Ignoring messages that come too fast
        } else {
          nextStatus = nextIsHigh;
          _lastEventTime = nextTiming;
          break;
        }
      } // Inner while

      if (nextStatus != _status) {
        // Serial.print("Got a change. Now status is "); Serial.println((int)_status);
        _status = nextStatus;
        _isPressed = nextStatus ^ _inverted;
        registerPressed();
        changed();
      } else {
        if (nextStatus ^ _inverted) {
          if (!_inHold) {
            if (micros() - _pressTime1 > LONG_CLICK_THRESHOLD) {
              _inHold = true;
              _hadFirstClick = false;
              hold();
              maybePublish("hold");
            }
          }
        } else {
          // Button not pressed. Was it a click-only ?
          if (_hadFirstClick) {
            if (micros() - _pressTime1 >= DOUBLE_CLICK_THRESHOLD) {
              _hadFirstClick = false;
              clickOnly();
              maybePublish("clickOnly");
            }
          }
        }
      } // No button state change
    } // Outer while

    // Since the timer wraps around, we may have a double/long events even for a single click
    // It requires plenty of bad luck, but can still happen
    // To prevent - when we're idle, keep the history rolling some long-enough time behind us
    if (micros() - _lastEventTime > 2 * IDLE_TIME) {
      Serial.print("Resetting Button");
      Serial.println(PIN);
      _pressTime1 = _pressTime2 = _lastEventTime = micros() - IDLE_TIME;
    }
  }

  void registerPressed() {
    // Register clicks, so we can later on decide if long-click, double-click, etc.
    // isPressed is the current state
    if (_isPressed) {
      // _updateTime1 is last release, _pressTime2 is the press before
      // Double-click ??
      if (_lastEventTime - _pressTime2 < DOUBLE_CLICK_THRESHOLD) {
        _isDblClick = true;
        _hadFirstClick = false;
      } else {
        // Raise a flag it wasn't a double-click. We may detect this flag in loop,
        // (if another click not coming), and know it was a single click
        _hadFirstClick = true;
      }
      pressed();
      maybePublish("pressed");
      if (_isDblClick) {
        doubleClicked();
        maybePublish("doubleClicked");
      }
    } else {
      // Button is raised: Maybe end of a long-click
      if (_inHold) {
        holdEnd();
        maybePublish("holdEnd");
      }
      released();
      maybePublish("released");
      _inHold = false;
      _isDblClick = false;
    }
    _pressTime2 = _pressTime1;
    _pressTime1 = _lastEventTime;
  }

  void maybePublish(const char *messages) {
    if (_publish) {
      publish(messages);
    }
  }

  virtual void publish(const char *message) {
    if (theComm != NULL) {
      Serial.print("Trying to publish '");
      Serial.print(message);
      Serial.println("'");
      theComm->publish(_ackTopic.c_str(), message);
    }
  }

  void print() {
    char msg[100];
    sprintf(msg, "Pin %d @ %d, state @ %d, _nextRead=%d, _nextWrite=%d, last change %d",
            (int)PIN, digitalRead(PIN) == HIGH, (int)_status, _nextRead, _nextWrite, _lastEventTime);
    Serial.println(msg);
  }
};


template <uint8_t PIN> long Button<PIN>::_timing[BUF_SIZE];
template <uint8_t PIN> bool Button<PIN>::_isHigh[BUF_SIZE];
template <uint8_t PIN> char Button<PIN>::_nextWrite;
template <uint8_t PIN> char Button<PIN>::_nextRead;
template <uint8_t PIN> bool Button<PIN>::_inverted;
template <uint8_t PIN> bool Button<PIN>::_inHold;
template <uint8_t PIN> bool Button<PIN>::_isDblClick;
template <uint8_t PIN> bool Button<PIN>::_hadFirstClick;
template <uint8_t PIN> char Button<PIN>::_status;
template <uint8_t PIN> long Button<PIN>::_lastEventTime;
template <uint8_t PIN> long Button<PIN>::_pressTime1;
template <uint8_t PIN> long Button<PIN>::_pressTime2;
template <uint8_t PIN> bool Button<PIN>::_isPressed;
template <uint8_t PIN> bool Button<PIN>::_publish;

#define MAKE_BUTTON(name, PIN, ackTopic)                \
  ICACHE_RAM_ATTR void BUTTON_HIT_##PIN();              \
  Button<PIN> name(&BUTTON_HIT_##PIN, ackTopic);        \
  void BUTTON_HIT_##PIN() { name.isr(); }

 
#endif // __BUTTON_H__
