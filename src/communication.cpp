#include "communication.h"
#include "loopManager.h"

WiFiClient espClient;

// This is the function that wraps the MQTT callback using the Comm 
void CommMasterCallback(char *topic, byte *payload, long length) {
  // Serial.println("At CommMasterCallback");
  theComm->masterCallBack(topic, payload, length);
}

Communication::Communication(int ledPin) :
  _mqttClient(espClient),
  _status(COMM_UNINITIALIZED),
  _wasConnected(false),
  _pLed(NULL),
  _callback(NULL),
  _numLoops(0),
  _totRT(0.),
  _cpuRT(0.),
  _rtMeasure(-1.)
{
  if (ledPin != -1) {
    // User can define a LED, which will indicate the communication status
    ledConfig.style = LedConfig::BLINK2;
    _pLed = new BlinkyLed(ledPin, ledConfig);
  }
}

void Communication::setup() {
  Serial.println("Setting WiFi and mqtt server");
  WiFi.begin(DCONFIG(NET_SSID), DCONFIG(PASSWORD));
  _mqttClient.setServer(DCONFIG(MQTT_SERVER), 1883);
  _mqttClient.setCallback(CommMasterCallback);
  _status = COMM_MQTT_CONNECTING;
}


void Communication::masterCallBack(char *topic, byte *payload, long length) {
  // Master callback. Will scan the list of sbscribers, and dispatch those who listen to this topic
  activated();
  // printMessage(topic, payload, length);
  const std::vector<Object *> pObjects = LoopManager::instance().getObjects();
  for (int i=0; i < pObjects.size(); i++) {
    const char *subsTopic = pObjects[i]->cmdTopic();
    // Serial.print("Checking against ");
    // Serial.println(subsTopic);
    if ((subsTopic != NULL) && strlen(subsTopic) && strcmp(topic, subsTopic) == 0) {
      // Serial.print("Matches (");
      // Serial.print(i);
      // Serial.print("), object of type ");
      // Serial.print(pObjects[i]->type());
      // Serial.print(" at ");
      // Serial.println((unsigned int) pObjects[i]);      
      pObjects[i]->onMessage(topic, payload, length);
    }
  }
}

void Communication::subscribeAll() {
  // Master callback. Will scan the list of sbscribers, and dispatch those who listen to this topic
  const std::vector<Object *> pObjects = LoopManager::instance().getObjects();
  Serial.print("Scanning ");
  Serial.print(pObjects.size());
  Serial.println(" objects:");
  for (int i=0; i < pObjects.size(); i++) {
    Object *pObj = pObjects[i];
    Serial.print(i);
    Serial.print(", type ");
    Serial.print(pObj->type());
    Serial.print(", at ");
    Serial.print((unsigned int)pObj);
    const char *ackTopic = pObj->ackTopic();
    if (strlen(ackTopic) > 0) {
      Serial.print(", Ack: ");
      Serial.print(ackTopic);
    }
    const char *cmdTopic = pObj->cmdTopic();
    if (strlen(cmdTopic) > 0) {
      Serial.print(", Subscribes to: ");
      Serial.print(cmdTopic);
      _mqttClient.subscribe(cmdTopic);
      pObj->onConnect();
    }
    Serial.println();
  }
  Serial.println("Finished subscribeAll");
}

void Communication::loop() {
  _lastStart = millis();
  if (needWait()) {
    return;
  }
  _mqttClient.loop();
  if (!mqttConnected()) {
    Serial.println("MQTT Not connected");
  }
  switch (_status) {
  case COMM_UP:
    if (_mqttClient.connected()) {
      _lastConnect = millis();
      break;
    } else {
      _status = COMM_MQTT_CONNECTING;
      Serial.println("MQTT connection lost");
    }
    
  case COMM_MQTT_CONNECTING: // Connect to MQTT
    if (WiFi.status() != 3) {
      Serial.print("Waiting for WiFi: ");
      Serial.println(DCONFIG(NET_SSID));
      ledConfig.style = LedConfig::BLINK;
      if (_pLed) {
        _pLed->setBrightness(LedConfig::MAX_VAL);
      }
      wait(500);
      break;
    } 
    
    Serial.print("Connecting MQTT: as ");
    Serial.print(DCONFIG(DEVICE_NAME));
    Serial.print(" from ");
    Serial.print(WiFi.localIP());
    Serial.print("/");
    Serial.print(WiFi.subnetMask());
    Serial.print(" : ");
    Serial.println(WiFi.gatewayIP());
    
    ledConfig.style = LedConfig::BLINK2;
    if (_pLed) {
      _pLed->setBrightness(LedConfig::MAX_VAL);
    }    
    
    if (_mqttClient.connect(DCONFIG(DEVICE_NAME), DCONFIG(MQTT_USER), DCONFIG(MQTT_PASS),
                            DCONFIG(DEVICE_STATUS_TOPIC), 0, true, DCONFIG(WILL_MESSAGE))) {
      Serial.println("MQTT Ok. Setting callback and subscribing");
      subscribeAll();
      std::string topic_str = makeTopic(DCONFIG(DEVICE_STATUS_TOPIC));
      publish(topic_str.c_str(), "In busyness", true);
      _status = COMM_UP;
      ledConfig.style = LedConfig::DECAY;
      if (_pLed) {
        _pLed->setBrightness(0);
      }      
      _wasConnected = true;      
    } else {
      Serial.print("MQTT connection failed, rc=");
      Serial.println(_mqttClient.state());
      wait(500);
      long timeSinceLastConnection = millis() - _lastConnect;
      if (_wasConnected && (timeSinceLastConnection > connectionLessThreshold)) {
        Serial.print("Lost connection for ");
        Serial.print(timeSinceLastConnection);
        Serial.println(" calling longTimeNoSeen()");
        longTimeNoSeen();
        _wasConnected = false;
        
        const std::vector<Object *> pObjects = LoopManager::instance().getObjects();
        for (int i=0; i < pObjects.size(); i++) {
          Object *pObj = pObjects[i];
          // Run subscriber's callback
          pObj->longTimeNoSeen();
        }
      }
      break;
    }
  }
}

void Communication::wait(long howMuch) {
  // Set the delay to the next operation. This will act like delay(), but not freeze the system
  _nextUpdate = millis() + howMuch;
}

bool Communication::needWait() {
  long tDiffTmp = _nextUpdate - millis();
  int tDiff = *((int*)(&tDiffTmp));
  return tDiff > 0;
}

bool Communication::wifiConnected() {
  // Not sure where this 3 is taken from...
  return WiFi.status() == 3;
}

bool Communication::mqttConnected() {
  if (_status == COMM_UNINITIALIZED) {
    return false;
  }
  return (_status != COMM_UNINITIALIZED) && _mqttClient.connected();
}

    
bool Communication::publish(const char *topic, const char *payload, bool retain) {
  if (topic == NULL || strlen(topic) == 0) {
    return false;
  }
  if (!mqttConnected()) {
    Serial.print("[Cannot] ");
  }
  Serial.print("Publish: ");
  Serial.print(topic);
  Serial.print(", ");
  Serial.println(payload);  
  if (!mqttConnected()) {
    return false;
  }
  activated();
  _mqttClient.publish(topic, payload, retain);
  if (_pLed) {
    _pLed->bump(LedConfig::MAX_VAL / 5);
  }
  return true;
}

// Global pointer to hold accessible Communication (or inherited) object
Communication *theComm=NULL;

