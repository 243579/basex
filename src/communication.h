#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

/**********************************
 * This is the "communication" module. 
 * Responsibilities:
 * 1. Connect to WiFi  & MQTT
 * 2. Detect long-time net breaks, so you can shut devices automatically (cannot via server, right?)
 * 3. Answer MQTT incoming messages. All subscriptions should go through Communication::subscribe
 * 4. Wrap publishing messages. It's good for you.
 */

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "blinkyLed.h"
#include "object.h"
#include "device_config.h"

class Communication {
public:
  static const long connectionLessThreshold = 120000;  // 2 minutes with no MQTT and we call longTimeNoSeen
private:
  WiFiClient _espClient;
  PubSubClient _mqttClient;
  int _status;
  bool _wasConnected; // Since last call to longTimeNoSeen
  long _lastConnect; // Triggers longTimeNoSeen
  enum status_t {COMM_UNINITIALIZED, COMM_MQTT_CONNECTING, COMM_UP};
  void wait(long howMuch);
  bool needWait();
  uint8 _ledPin;
  LedConfig ledConfig;
  BlinkyLed *_pLed;
  void (*_callback) (char *, byte*, long);
  long _nextUpdate;
  int _numLoops;
  long _totRT;
  long _cpuRT;
  long _lastStart;
  float _rtMeasure;
  
public:
  Communication(int ledPin=DeviceConfig::NO_LED);
  void setup();
  void masterCallBack(char *topic, byte *payload, long length);
  void subscribeAll();
  void setCallback(void (*callback)(char *, byte*, long)); // Set a callback to be called on every topic
  void loop(); // Don't forget to call this one from your loop
  bool wifiConnected();
  bool mqttConnected();
  bool publish(const char *topic, const char *payload, bool retain=false);
  virtual void activated() {} // Called every publish or receive
  virtual void longTimeNoSeen() {} // To be called if disconnected from network for a long time, and need to shut down devices
};

extern Communication *theComm;


#endif // __COMMUNICATION_H__
