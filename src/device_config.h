

#ifndef __DEVICE_CONFIG_H__
#define __DEVICE_CONFIG_H__


struct DeviceConfig {
  // Holds the device data. Including passwords.
  // Make sure to not commit data of this struct unless you really wanted to
  // Proper minimal usage is something like thisP:
  // DeviceConfig::instance() = DeviceConfig("dev-unit", "wifiname", "wifipass", "192.168.20.222");
  static const int NO_LED = -1;
  const char *DEVICE_NAME;         // test-dev
  const char *NET_SSID;            // mywify-wifi
  const char *PASSWORD;            // wifipassword
  const char *MQTT_SERVER;         // 192.168.20.222
  const char *MQTT_USER;           // mq_user
  const char *MQTT_PASS;           // mq_pass
  int LED_PIN;                     // LED_BUILTIN
  const char *WILL_MESSAGE;        // gone
  const char *DEVICE_STATUS_TOPIC; // status

  static DeviceConfig &instance() {
    static DeviceConfig theConfig;
    return theConfig;
  }

  static void set(const char *deviceName  =NULL  , const char *wifiName=NULL, const char *wifiPass=NULL,
                  const char *mqttHostname=NULL  , const char *mqttUser=NULL, const char *mqttPass=NULL,
                  int ledPin=DeviceConfig::NO_LED,
                  const char *willMessage ="gone", const char *deviceStatusTopic="status") {
    DeviceConfig &dc = DeviceConfig::instance();
    dc.DEVICE_NAME         = deviceName        ;
    dc.NET_SSID            = wifiName          ;
    dc.PASSWORD            = wifiPass          ;
    dc.MQTT_SERVER         = mqttHostname      ;
    dc.LED_PIN             = ledPin            ;
    dc.MQTT_USER           = mqttUser          ;
    dc.MQTT_PASS           = mqttPass          ;
    dc.WILL_MESSAGE        = willMessage       ;
    dc.DEVICE_STATUS_TOPIC = deviceStatusTopic ;
  }  
};





#define DCONFIG(X) DeviceConfig::instance().X

#endif // __DEVICE_CONFIG_H__


