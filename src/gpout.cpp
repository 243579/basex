#include "gpout.h"

template <> void GPOut<bool>::setMyLimits() {
  GPOut<bool>::setLimits(false, true, WRAP_AROUND);
}

template <> void GPOut<char>::setMyLimits() {
  GPOut<char>::setLimits(0, 255, BOUNDED);
}

template <> void GPOut<float>::setMyLimits() {
  GPOut<float>::setLimits(0, 1, BOUNDED);
}

template <> void GPOut<bool>::write() {
  // Bypass the analogWrite and the writeInt thingies
  digitalWrite(_pin, _inverted ^ GPOut<bool>::value());
}

template <> void GPOut<char>::write() {
  // Normalize from char to 0-1023 int, maybe invert later
  writeInt(4 * (int) GPOut<char>::value());
}

template <> void GPOut<float>::write() {
  writeInt((int) (1023 * GPOut<float>::value()));
}
