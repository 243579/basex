//TODO: Seems like onLongDisconnect does not work... Need to call write() after setting the value
#ifndef __GPOUT_H__
#define __GPOUT_H__

#include "variable.h"

template <typename T>
class GPOut : public Variable<T> {

public:
  static const int MAX_PIN_VAL=1023;
  
private:
  bool _inverted;
  int _pin;

public:
  GPOut(int pin, const char *cmdTopic, const char *ackTopic, T defaultValue=(T) 0, bool inverted=true, bool resetOnCommLoss=true) :
    Variable<T>(defaultValue, cmdTopic, ackTopic, /* pWhere= */ NULL, resetOnCommLoss),
    _pin(pin),
    _inverted(inverted)
  {
    pinMode(_pin, OUTPUT);
    setMyLimits();
    write();
  }

  virtual const char *type() const {return "GP-Out-Variable";}
  
  virtual void setMyLimits() {
    this->setLimits((T)0, (T)MAX_PIN_VAL, BOUNDED);
  }

  virtual void onChange(bool reallyChanged) {
    write();
    Variable<T>::publish();
    if (reallyChanged) {
      Variable<T>::changed(); // Notify the LoopManager
    }
  }

  virtual void write() {
    GPOut<T>::writeInt((int) GPOut<T>::value());
  }

  virtual void writeInt(int intValue) {
    if (_inverted) {
      intValue = MAX_PIN_VAL - intValue;
    }
    analogWrite(_pin, intValue);
  }
  
};

typedef GPOut<bool  > BoolGPOut  ;
typedef GPOut<int   > IntGPOut   ;
typedef GPOut<float > FloatGPOut ;

#endif // __GPOUT_H__




