#include <assert.h>
#include "loopManager.h"
#include "communication.h"

LoopManager::LoopManager() :
  _totRT(0),
  _fps(0),
  _numLoops(0),
  _pushing(0)
{
  _lastStart = millis();
}

LoopManager &LoopManager::instance() {
  static LoopManager myLooper;
  return myLooper;
}

void LoopManager::setup(const char *deviceName  , const char *wifiName, const char *wifiPass,
                        const char *mqttHostname, const char *mqttUser, const char *mqttPass,
                        int ledPin, int baudRate,
                        const char *willMessage , const char *deviceStatusTopic) {
  static bool alreadyInit=false;
  if (alreadyInit != false) {
    Serial.println("You should not access LoopManager::setup more than once");
    assert(false);
  }
  
  if (baudRate) {
    Serial.begin(baudRate);
    delay(500);  // Make Serial really begin
    Serial.print("At LoopManager::setup. Device name: ");
    Serial.println(deviceName);
  }

  alreadyInit = true;
  
  if (wifiName != NULL) {
    DeviceConfig::set(deviceName, wifiName, wifiPass,
                      mqttHostname, mqttUser, mqttPass, ledPin,
                      willMessage, deviceStatusTopic);
    theComm = new Communication(ledPin);
    theComm->setup();
  }

  for (int i=0; i < _pObjects.size(); i++ ) {
    _pObjects[i]->setup();
  }
  Serial.print("LoopManager has ");
  Serial.print(_pObjects.size());
  Serial.print(" variables and ");
  Serial.print(_connection.size());
  Serial.println(" connections.");  
}

void LoopManager::connect(Object &srcObject, Signal_t signal, Object &dstObject, Slot_t slot) {
  _connection.push_back(Connection(srcObject, signal, dstObject, slot));
}

void LoopManager::registerObject(Object &object) {
  // Add a handler for a topic. Also register its onMessage if it has a topic
  _pObjects.push_back(&object);
  Serial.print("Registering. Now got this: ");
  Serial.println(_pObjects.size());
  //const char *topic = object.topic();
}

void LoopManager::loop() {
  // Main loop runner. Will call all objects' loop functions, and measure time
  _lastStart = millis();
  if (theComm != NULL) {
    theComm->loop();
  }
  for (int i=0; i < _pObjects.size(); i++ ) {
    _pObjects[i]->loop();
  }
  executeCallBacks();
  measureFPS();
  if (LoopManager::instance().canReadRt()) {
    Serial.print("LoopManager: ");
    Serial.print(millis());
    Serial.print(", FPS = ");
    Serial.println(LoopManager::instance().fps());
  }  
}

void LoopManager::measureFPS() {
    
  // This function also adds a small delay, so we don't kill the chip
  long idleTime = 10;
  delay(idleTime);  
  _totRT += millis() - _lastStart;
  _numLoops ++;
  if (_numLoops == LOOPS_FOR_RT) {
    // This calculation's time is neglected from the RT calculation
    if (_totRT != 0) {
      _fps = ((float) _numLoops) / ((float) _totRT) * 1000;
    } else {
      _fps = -1;
    }
    _numLoops = 0;
    _totRT = 0;
  }
}

bool LoopManager::callBack(Object *pSrcObject, Signal_t signal) {
  // Didn't really need the bool return value. Enables one-liner in callers. We return if callback was found
  // Serial.print("LoopManager: Adding pending operation ");
  // Serial.print(signalName(signal));
  // Serial.print(" from object ");
  // Serial.print(pSrcObject->type());
  // Serial.print(" in ");
  // Serial.println((unsigned int)pSrcObject);
  for (int i=0; i < _connection.size(); i++) {
    Connection &conn = _connection[i];
    if ((conn.pSrcObj == pSrcObject) && (conn.signal == signal)) {
      _pendingOperations[_pushing].push_back(i);
      return true;
    }
  }
  return false;
}

void LoopManager::executeCallBacks() {
  // When the frame is over, we execute all the pending callbacks, and switch buffers
  int executing = _pushing;
  _pushing = 1 - _pushing;
  for (int i=0; i < _pendingOperations[executing].size(); i++ ) {
    Connection &conn = _connection[_pendingOperations[executing][i]];
    Serial.print(i);
    Serial.print(" LoopManager: Executing ");
    Serial.print(conn.pSrcObj->type());
    Serial.print(" (");
    Serial.print((unsigned int) conn.pSrcObj);
    Serial.print(" ):");
    Serial.print(signalName(conn.signal));
    Serial.print(" --> ");
    Serial.print(conn.pDstObj->type());
    Serial.print(" (");
    Serial.print((unsigned int) conn.pDstObj);
    Serial.print(" ):");
    Serial.println(slotName(conn.slot));
    conn.call();
  }
  _pendingOperations[executing].clear();
}
