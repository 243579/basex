
#ifndef __LOOPMANAGER_H__
#define __LOOPMANAGER_H__
#include <vector>
#include "object.h"
#include "device_config.h"

class LoopManager {
private:
  LoopManager();
  std::vector<Object *> _pObjects; // All objects, regardless of MQTT
  std::vector<Connection> _connection;
  std::vector<int> _pendingOperations[2]; // Double buffer of callbacks we accumulate in the frame
  long _totRT;
  long _lastStart;
  float _fps;
  long _numLoops;
  int _pushing;

public:
  static const int LOOPS_FOR_RT=1000;
  
  static LoopManager &instance();

  void setup(const char *deviceName  =NULL  , const char *wifiName=NULL, const char *wifiPass=NULL,
             const char *mqttHostname=NULL  , const char *mqttUser=NULL, const char *mqttPass=NULL,
             int ledPin=BUILTIN_LED         , int baudRate=115200,
             const char *willMessage ="gone", const char *deviceStatusTopic="status");

  void connect(Object &srcObject, Signal_t signal, Object &dstObject, Slot_t slot);
  const std::vector<Object *> &getObjects() const { return _pObjects; }
  
  void registerObject(Object &object);
  void loop();
  void measureFPS(); // For stat... See how busy we are
  bool callBack(Object *pSrcObject, Signal_t signal);
  bool canReadRt() { return _numLoops == 0;}
  float fps () const { return _fps; }
  
private:
  void executeCallBacks();


};

#endif // __LOOPMANAGER_H__




