#include "object.h"
#include "loopManager.h"

double patch::str2double(const byte *payload, long length, double defaultVal) {
  static char temp[30];
  if (length < sizeof(temp) / sizeof(temp[0]) - 1) {
    memcpy(temp, payload, length);
    temp[length] = '\0';
    return strtod(temp, NULL);
  }
  return defaultVal;
}

Object::Object(const char *cmdTopic, const char *ackTopic) :
  _inCmdTopic(cmdTopic),
  _inAckTopic(ackTopic)
{
  LoopManager::instance().registerObject(*this);
}

void Object::setup() {
  if (_inCmdTopic != NULL) {
    _cmdTopic = makeTopic(_inCmdTopic);
  }
  if (_inAckTopic != NULL) {
    _ackTopic = makeTopic(_inAckTopic);
  }
}

void Object::onMessage(const char *topic, const byte *payload, long length) {
  Serial.print("Oops... You're at Object::onMessage (");
  Serial.print(type());
  Serial.print(" at ");
  Serial.print((unsigned int) this);
  Serial.println(")");
  printMessage(topic, payload, length);
}

const char *slotName(Slot_t slot) {
  switch (slot) {
  case Slot_t::SET              : return "SET";
  case Slot_t::RESET            : return "RESET";
  case Slot_t::PUBLISH          : return "PUBLISH";
  case Slot_t::INCREMENT        : return "INCREMENT";
  case Slot_t::DECREMENT        : return "DECREMENT";
  case Slot_t::CONNECT          : return "CONNECT";
  case Slot_t::LONG_NO_SEE      : return "LONG_NO_SEE";
  case Slot_t::USER_1           : return "USER_1";
  case Slot_t::USER_2           : return "USER_2";
  case Slot_t::USER_3           : return "USER_3";

  case Slot_t::NUM_SLOTS        : return "NUM_SLOTS";
  default                       : return "Unknown Slot. Bug ???";
  }
}

const char *signalName(Signal_t signal) {
  switch(signal) {
    case Signal_t::CLICK_ONLY   : return "CLICK_ONLY";
    case Signal_t::DOUBLE_CLICK : return "DOUBLE_CLICK";
    case Signal_t::PRESSED      : return "PRESSED";
    case Signal_t::RELEASED     : return "RELEASED";
    case Signal_t::HOLD         : return "HOLD";
    case Signal_t::HOLD_END     : return "HOLD_END";
    case Signal_t::CHANGED      : return "CHANGED";
    case Signal_t::NUM_SIGNALS  : return "NUM_SIGNALS";
  default                       : return "Unknown Signal. Bug ???";
  }
}


void Object::clickOnly()     {LoopManager::instance().callBack(this, Signal_t::CLICK_ONLY   );}
void Object::doubleClicked() {LoopManager::instance().callBack(this, Signal_t::DOUBLE_CLICK );}
void Object::pressed()       {LoopManager::instance().callBack(this, Signal_t::PRESSED      );}
void Object::released()      {LoopManager::instance().callBack(this, Signal_t::RELEASED     );}
void Object::hold()          {LoopManager::instance().callBack(this, Signal_t::HOLD         );}
void Object::holdEnd()       {LoopManager::instance().callBack(this, Signal_t::HOLD_END     );}
void Object::changed()       {LoopManager::instance().callBack(this, Signal_t::CHANGED      );}
