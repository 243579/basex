// Base class for everything that wants to register at the Communication object's MQTT handling list
// If, for your own sake, you want to react to all topics, make the 

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <Arduino.h>

#include "device_config.h"

#include <string>
#include <sstream>

// Here are some things that didn't have enough mass to get a file of their own...
namespace patch
{
  // - - - Taken from https://stackoverflow.com/questions/12975341/to-string-is-not-a-member-of-std-says-g-mingw
  template < typename T > std::string to_string( const T &n)
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }

  double str2double(const byte *payload, long length, double defaultVal=0);

}

#define CLIP(X, V_MIN, V_MAX) ((X) < (V_MIN) ? (V_MIN) : ((X) > (V_MAX) ? (V_MAX) : (X)))

void printMessage(const char *topic, const byte *payload, long length);

// End of such masless things - - -

static inline std::string makeTopic(const char *topic) {
  if (topic == NULL) {
    return std::string("");
  }
  std::string dest("/");
  dest += DCONFIG(DEVICE_NAME);
  if (topic[0] != '/') {
    dest += "/";
  }
  dest += topic;
  // Serial.print("makeTopic: ");
  // Serial.println(dest.c_str());
  return dest;
}

enum class Slot_t {SET,         // Variable, GPIO, Screen? Encoder ?
                   PUBLISH,     // Everyone, but need to set topic
                   INCREMENT,   // Variable, Encoder ? GPIO ?
                   DECREMENT,   // Variable, Encoder ? GPIO ?
                   CONNECT,     // All. Called when MQTT is re/connected
                   LONG_NO_SEE, // All stateful. Call when MQTT is disconnected for too long
                   RESET,       // Set variable to initial value
                   USER_1,      // Any user-defined slot
                   USER_2,      // Any user-defined slot
                   USER_3,      // Any user-defined slot
                   NUM_SLOTS
};

enum class Signal_t {CLICK_ONLY,   // Button (Called when we know it's not a double click
                     DOUBLE_CLICK, // Button
                     PRESSED,      // Button
                     RELEASED,     // Button
                     HOLD,         // Button
                     HOLD_END,     // Button
                     CHANGED,      // Button, Variable, GPIO, Encoder ?
                     NUM_SIGNALS
};

const char *slotName(Slot_t slot);
const char *signalName(Signal_t signal);

class Object {
  
private:
  const char *_inCmdTopic; // To hold the string between CTor and setup
  const char *_inAckTopic; // To hold the string between CTor and setup
  
protected:
  std::string _cmdTopic; // NOTE: Topic prefix automatically added!
  std::string _ackTopic; // NOTE: Topic prefix automatically added!

  
public:

  Object(const char *cmdTopic=NULL, const char *ackTopic=NULL);

  void setTopics(std::string name) {
    setCmdTopic((name + std::string("/cmd")).c_str());
    setAckTopic((name + std::string("/ack")).c_str());
  }

  void setAckTopic(const char *topic) {
    // Add the prefix to the topic, and set in the class memeber
    _ackTopic = makeTopic(topic);
  }
  
  void setCmdTopic(const char *topic) {
    // Add the prefix to the topic, and set in the class memeber
    _cmdTopic = makeTopic(topic);
  }

  const char *cmdTopic() const {
    return _cmdTopic.c_str();
  }

  const char *ackTopic() const {
    return _ackTopic.c_str();
  }

  virtual const char *type() const {return "Abstract object" /*".. Should not see this?" */;}

  // Callback for MQTT messages. Override and do as you will.
  virtual void onMessage(const char *topic, const byte *payload, long length);

  // For some stateful objects: Button, ...
  virtual void loop() {}

  // The things we do in setup... Mostly related to DeviceConfig
  virtual void setup();

  // Callback after MQTT connected/restored connection
  virtual void onConnect(Object *other=NULL)        {}

  // Callback after MQTT long time not seen
  virtual void onLongDisconnect(Object *other=NULL) {}
  
  virtual void publish(Object *other=NULL)          {Serial.println("publish Not properly inherited");}
  virtual void setFrom(Object *other=NULL)          {Serial.println("setFrom Not properly inherited");}
  virtual void longTimeNoSeen(Object *other=NULL)   {Serial.println("longTimeNoSeen Not properly inherited");}
  virtual void increment(Object *other=NULL)        {Serial.println("increment Not properly inherited");}
  virtual void decrement(Object *other=NULL)        {Serial.println("decrement Not properly inherited");}
  virtual void reset(Object *other=NULL)            {}
  virtual void user_1(Object *other=NULL)           {}
  virtual void user_2(Object *other=NULL)           {}
  virtual void user_3(Object *other=NULL)           {}

  // These are all possible signals from all inheritants: 
  virtual void clickOnly()     ;
  virtual void doubleClicked() ; 
  virtual void pressed()       ;
  virtual void released()      ;
  virtual void hold()          ;
  virtual void holdEnd()       ;
  virtual void changed()       ;
};

struct Connection {
  Object  *pSrcObj;
  Object  *pDstObj;
  Signal_t signal ;
  Slot_t   slot   ;
  Connection(Object &in_srcObj, Signal_t in_signal, Object &in_dstObj, Slot_t in_slot):
    pSrcObj (&in_srcObj),
    pDstObj (&in_dstObj),
    signal  (in_signal ),    
    slot    (in_slot   )
    {}

  bool call() {
    switch (slot) {
    case Slot_t::SET         : pDstObj->setFrom(pSrcObj) ; break;
    case Slot_t::PUBLISH     : pDstObj->publish()        ; break;
    case Slot_t::INCREMENT   : pDstObj->increment()      ; break;
    case Slot_t::DECREMENT   : pDstObj->decrement()      ; break;
    case Slot_t::RESET       : pDstObj->reset()          ; break;
    case Slot_t::LONG_NO_SEE : pDstObj->longTimeNoSeen() ; break;
    case Slot_t::USER_1      : pDstObj->user_1()         ; break;
    case Slot_t::USER_2      : pDstObj->user_2()         ; break;
    case Slot_t::USER_3      : pDstObj->user_3()         ; break;
    default:
      return false;
    }
    return true;
  }
};


#endif // __OBJECT_H__




