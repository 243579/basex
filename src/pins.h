// This file defines the text-on-chip to real numbers mapping.
// Never was sure why it's so complicated...
// Tested on Wemos D1. Note that some of the pins should be high/low for boot

#ifndef __PINS_H__
#define __PINS_H__


/* Working GPIO with ISRs:
   (Symbol = Arduino name)

   Symbol   Value   Text on chip  Comments
   D0        3        RX           Not working
   D1        1        TX           Not working
   D2        16       D0           Not working
   D3        5        D1           Ok
   D4        4        D2           Ok
   D5        14       D5           Ok
   D6        12       D6           Ok
   D7        13       D7           Ok
   D8        0        D3           Ok (But don't connect a LED between it and VCC!)
   D9        2        D4           Ok (Builtin LED)
   D10       15       D8           Not working, disconnect for flashing
 */

#define PIN_RX 3
#define PIN_TX 1
#define PIN_D0 16
#define PIN_D1 5
#define PIN_D2 4
#define PIN_D5 14
#define PIN_D6 12
#define PIN_D7 13
#define PIN_D3 0
#define PIN_D4 2
#define PIN_D8 15

#endif // __PINS_H__
