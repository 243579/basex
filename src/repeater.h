/****************************
 * A simple class to make things repeat every here and there
 * To use instanciate a global/statuc Repeater, and if (repeater.loop()) { do_something(); }
 * Time units in mSec
 *****************************/

#ifndef __REPEATER_H__
#define __REPEATER_H__

#include <Arduino.h>

class Repeater {
private:
  long _lastRun;
  
public:  
  long interval;
  bool enabled;
  bool oneShot;
  
  Repeater(long in_interval, bool in_enabled=true, bool in_oneShot=false) {
    interval = in_interval;
    enabled = in_enabled;
    oneShot = in_oneShot;
    _lastRun = millis();
  }

  bool loop() {
    if (!enabled) {
      return false;
    }
    long now = millis();
    long dTime = now - _lastRun;
    if (dTime >= interval) {
      _lastRun = now;
      if (oneShot) {
        enabled = false;
      }
      return true;
    }
    return false;
  }

  void cancel() {
    enabled = false;
  }

  void start(long newInterval=0) {
    _lastRun = millis();
    enabled = true;
    if (newInterval != 0) {
      interval = newInterval;
    }
  }

};


#endif // __REPEATER_H__
