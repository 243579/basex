#include <ArduinoJson.h>
#include "rgbLed.h"


RGBLed::RGBLed(uint8 rPin, uint8 gPin, uint8 bPin, const char *cmdTopic, const char *ackTopic):
  Object(cmdTopic, ackTopic),
  rLed(rPin, ledConfig, 0),
  bLed(gPin, ledConfig, 0),
  gLed(bPin, ledConfig, 0)
{
    const char *initCycleColors [] = {
    "00007F", // dark blue
    "0000FF", // blue
    "007FFF", // azure
    "00FFFF", // cyan
    "7FFF7F", // light green
    "FFFF00", // yellow
    "FF7F00", // orange
    "FF0000", // red
    "7F0000"  // dark red
  };

  for (int i=0; i<CYCLE_NUM_COLORS; i++) {
    _colorCycle[i].parseHex(initCycleColors[i]);
    char buf[50];
    sprintf(buf, "%d: %d %d %d\n", i, _colorCycle[i].red, _colorCycle[i].grn, _colorCycle[i].blu);
    Serial.print(buf);
  }
}

void RGBLed::loop() {
  // On or off ?
  ledConfig.style = effectMode == RGB_CYCLE ? LedConfig::TRANS : effectMode;
  

  if (effectMode == RGBLed::RGB_CYCLE) {
    long dTime = millis() - _lastCycleChange;
    if (dTime > 2000) {
      _lastCycleChange = millis();
      _curColor = (_curColor + 1 ) % CYCLE_NUM_COLORS;
      OneColor &col = _colorCycle[_curColor];
      rPower = col.red;
      gPower = col.grn;
      bPower = col.blu;
      publish();
    }

  }
  
  // Set the actual BlinkyLed objects
  rLed.setBrightness(rPower * brightness / 256);
  gLed.setBrightness(gPower * brightness / 256);
  bLed.setBrightness(bPower * brightness / 256);
}

void RGBLed::onMessage(const char *topic, const byte *payload, long length) {
  // Expected payload example:
  //    {"state":"ON","color":{"r":255,"g":163,"b":135}, "brightness":156, "effect":"trans"}
  StaticJsonDocument<300> doc;
  DeserializationError error = deserializeJson(doc, payload, length);

  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }

  bool isOn = ledConfig.style != LedConfig::OFF;
  
  if (doc.containsKey("state")) {
    const char* state = doc["state"];
    isOn = parseBool((const byte *)state, strlen(state));
    Serial.print("New isOn: "); Serial.println(isOn);
    effectMode = isOn ? LedConfig::ON : LedConfig::OFF;
  } else {
    Serial.println("No state");
  }

  if (doc.containsKey("color")) {
    rPower = (uint8) doc["color"]["r"];
    gPower = (uint8) doc["color"]["g"];
    bPower = (uint8) doc["color"]["b"];
    Serial.print("New color: ");
    Serial.print(rPower); Serial.print(",");
    Serial.print(gPower); Serial.print(",");
    Serial.println(bPower);
  } else {
    Serial.println("No color");
  }

  if (doc.containsKey("brightness")) {
    brightness = (uint8) doc["brightness"];
    Serial.print("New brightness: ");
    Serial.println(brightness);
  } else {
    Serial.println("No brightness");
  }

  if (doc.containsKey("effect")) {
    // This should match the LedConfig::style, plus the RGBLed::RGB_CYCLE
    static const int numEffects = sizeof(effectsList) / sizeof(effectsList[0]);
    const char *effect = doc["effect"];
    int effectNum = inStringList((const byte *)effect, strlen(effect), effectsList, numEffects);
    if (effectNum != -1) {
      Serial.print("New effect: ");
      Serial.println(effectsList[effectNum]);
      effectMode = (uint8) effectNum;
    }
  } else {
    Serial.println("No effect");
  }

  publish();
}

void RGBLed::publish(Object *other) {
  if (theComm != NULL) {
    std::string output;
    StaticJsonDocument<300> doc;
    doc["state"] = effectMode != LedConfig::OFF ? "ON" : "OFF";
    JsonObject color = doc.createNestedObject("color");
    color["r"] = rPower;
    color["g"] = gPower;
    color["b"] = bPower;
    doc["brightness"] = brightness;
    doc["effect"] = effectsList[effectMode];

    serializeJson(doc, output);
    theComm->publish(_ackTopic.c_str(), output.c_str());
  }
}
