#ifndef __RGBLED_H__
#define __RGBLED_H__

#include "utils.h"
#include "variable.h"
#include "blinkyLed.h"
#include "object.h"

static const char *effectsList[] = {"OFF", "ON", "BLINK", "BLINK2", "DECAY", "TRIG", "TRANS", "CYCLE"};

class RGBLed : public Object {
  //
  // This class represents a 3-PIN RGB led.
  // Expecting Hass to send command in the following manner:
  //    {"state":"ON","color":{"r":255,"g":163,"b":135}, "brightness":156, "effect":"trans"}
  // Each such command updates internal state where it has data
  // Ack sends full state always.
  // Keep in mind that since HASS doesn't always send full state, the MQTT retention is not
  // effective in case of communication loss (shrug)
  //
  // This comes from hass yml file like this:
  //  - schema: json
  //    name: rgb_led
  //    state_topic: "/RGB_LED/get"
  //    command_topic: "/RGB_LED/cmd"
  //    brightness: true
  //    rgb: true
  //    effect: true
  //    effect_list: [name1, name2, trans]
  //    optimistic: false
  //    qos: 0

  static const int RGB_CYCLE = LedConfig::NUM_STYLES;
  static const int CYCLE_NUM_COLORS = 9;

  
private:
  BlinkyLed rLed;          // Handler of R LED
  BlinkyLed gLed;          // Handler of G LED
  BlinkyLed bLed;          // Handler of B LED

  OneColor _colorCycle[CYCLE_NUM_COLORS]; // So we can autonomously cycle colors
  int _curColor;                          // Current pointer when cycling
  long _lastCycleChange;                  // To know when to change color
  long _cycleStatePeriod;                 // How long to stay on each mode of the cycle

  
public:
  uint8 brightness;
  uint8 effectMode;
  uint8 rPower;
  uint8 gPower;
  uint8 bPower;
  
  LedConfig ledConfig;     // Common to all colors
  
  RGBLed(uint8 rPin, uint8 gPin, uint8 bPin, const char *cmdTopic=NULL, const char *ackTopic=NULL);

  virtual void loop();
  virtual void onMessage(const char *topic, const byte *payload, long length);
  virtual void publish(Object *other=NULL);
  virtual const char *type() const {return "RGB Led";}
  

};

#endif // __RGBLED_H__




