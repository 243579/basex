#include "object.h"

void printBytes(const byte *msg, int length) {
  for (int i=0; i<length; i++) {
    Serial.print((char)msg[i]);    
  }  
}

void printMessage(const char *topic, const byte *payload, long length) {
  Serial.print("Message: len=");
  Serial.print(length);
  Serial.print(", ");
  Serial.print(topic);
  Serial.print(", ");
  printBytes(payload, length);
  Serial.println("");
}

// bool stringEq(const char *str1, const char *str2, int max_len) {
//   int len1 = strlen(str1);
//   int len2 = strlen(str2);
//   if ((len1 != len2) || (len1 > max_len) || (len2 > max_len)) {
//     return false;
//   }  
//   for (int i=0; i < len1; i++) {
//     if (str1[i] != str2[i]) {
//       return false;
//     }
//   }
//   return true;
// }
