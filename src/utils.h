
#ifndef __UTILS_H__
#define __UTILS_H__

#include <Arduino.h>

static int hex2dec(const char x) {
  int res = 0;
  if ('0' <= x && x <= '9') {
    return x - '0';
  }
  if ('a' <= x && x <= 'f') {
    return 10 + x - 'a';
  }
  if ('A' <= x && x <= 'F') {
    return 10 + x - 'A';
  }
  return 0;
}

static int hex2dec255(const char x[2]) {
  return hex2dec(x[0]) * 16 + hex2dec(x[1]);
}

struct OneColor {
  // Red Green Blue brighNess
  uint8 red;
  uint8 grn;
  uint8 blu;
  
  void parseHex(const char ptr[6]) {
    red = hex2dec255(&ptr[0]);
    grn = hex2dec255(&ptr[2]);
    blu = hex2dec255(&ptr[4]);
  }

  bool parseCommaSep(const char *buffer, const int maxLen) {
    char *pos = (char *) buffer;
    long r, g, b;
    r = strtol(pos, &pos, 10);
    g = strtol(pos + 1, &pos, 10);
    b = strtol(pos + 1, &pos, 10);
    if (r == (uint8) r &&
        g == (uint8) g &&
        b == (uint8) b &&
        pos < buffer + maxLen) {
      red = (uint8) r;
      grn = (uint8) g;
      blu = (uint8) b;
      return true;
    } else {
      Serial.print("OneColor::parseCommaSep: Error parsing message: ");
      Serial.println(buffer);
    }
    return false;
  }

};

static inline int inStringList(const byte *payload, long length, const char **ppList, int listLen) {
  for (int i=0; i < listLen; i++) {
    if (strncasecmp((const char *)payload, ppList[i], length) == 0) {
      return i;
    }
  }
  return -1;
}

static inline bool parseBool(const byte *payload, long length) {
  static const char * trues[] = { "on" , "1", "yes", "enabled", "true"};
  return inStringList(payload, length, trues, sizeof(trues) / sizeof(trues[0])) != -1;
}



#endif // __UTILS_H__




