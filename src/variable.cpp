#include "variable.h"
#include "utils.h"

// Specification of some functions: parse, increment, decrement
template <>
bool Variable<bool>::parse(const byte *payload, long length) {
  return parseBool(payload, length);
}

template <> void Variable<bool >::setFrom  (Object *pOther) { setValue(!_defaultValue  ); }
template <> void Variable<bool >::increment(Object *pOther) { setValue(!(*_pValue)     ); }
template <> void Variable<bool >::decrement(Object *pOther) { setValue(!(*_pValue)     ); }
template <> void Variable<int  >::increment(Object *pOther) { setValue((*_pValue) + 1  ); }
template <> void Variable<int  >::decrement(Object *pOther) { setValue((*_pValue) - 1  ); }
template <> void Variable<float>::increment(Object *pOther) { setValue((*_pValue) + 1. ); }
template <> void Variable<float>::decrement(Object *pOther) { setValue((*_pValue) - 1. ); }
