
#ifndef __VARIABLE_H__
#define __VARIABLE_H__

#include <string.h>
#include <string>
#include <stdlib.h>     /* strtol */
#include "communication.h"
#include "loopManager.h"
#include "object.h"

enum {UNLIMITED, BOUNDED, WRAP_AROUND};

template <typename T>
class Variable : public Object {
  T _value;
  T *_pValue;
  T _defaultValue;
  T _minValue;
  T _maxValue;
  bool _resetOnCommLoss;
  int _limitType;
public:
  Variable(T defaultValue, const char *cmdTopic=NULL, const char *ackTopic=NULL, T *pWhere=NULL, bool resetOnCommLoss=true):
    Object(cmdTopic, ackTopic),
    _defaultValue(defaultValue),
    _resetOnCommLoss(resetOnCommLoss),
    _limitType(UNLIMITED)
  {
    postCTor();
  }

  virtual const char *type() const {return "Variable";}

  void setLimits(T minValue, T maxValue, int limitType=BOUNDED) {
    _minValue = minValue;
    _maxValue = maxValue;
    _limitType = limitType;
  }

  // Commong code for both CTors
  void postCTor() {
    pointTo(&_value);
    setValue(_defaultValue);
  }

  // Set working variable on other's memory, so you don't need to inherit and implement onChange()
  void pointTo(T *pWhere) {
    if (pWhere == NULL) {
      pWhere = &_value;
    }
    _pValue = pWhere;
  }

  // Return a sane value for newValue
  virtual T validate(T newValue) {
    if (_limitType == BOUNDED) {
      return CLIP(newValue, _minValue, _maxValue);
    }
    if (_limitType == WRAP_AROUND) {
      if (_minValue == _maxValue) {
        return _minValue;
      }
      if ((newValue < _minValue) || (newValue >= _maxValue)) {
        return wrapAround(newValue);
      }
    }
    return newValue;
  }

  // In numbers we modulo. In others? Don't know...
  virtual T wrapAround(T newValue) {
    float range = (float) (_maxValue - _minValue);
    float quantsMissing = ((float)(newValue - _minValue) / range);
    int numRangesMissing = (int) (quantsMissing < 0 ? quantsMissing - 1 : quantsMissing);  // int(floor(quantsMissing))
    return newValue - range * numRangesMissing;
  }
  
  void setValue(const T &newValue) {
    T temp = validate(newValue);
    bool reallyChanged = *_pValue != temp;
    *_pValue = temp;
    onChange(reallyChanged);
  }

  // Will be called when the variable is set from T
  void operator = (const T &newValue) {
    setValue(newValue);
  }

  // Will be called when the variable is casted to T
  T operator () () const {
    return *_pValue;
  }

  virtual T value() const {
    return *_pValue;
  }

  virtual void publish() {
    if (theComm != NULL) {
      Serial.print("Trying to publish '");
      Serial.print(toString().c_str());
      Serial.println("'");
      theComm->publish(_ackTopic.c_str(), toString().c_str());
    }
  }

  // Called when an MQTT is established
  virtual void onConnect() {
    publish();
  }

  // User defined function for whatever... But first see pointTo
  virtual void onChange(bool reallyChanged) {
    publish();
    if (reallyChanged) {
      changed(); // Notify the LoopManager
    }
  }

  // Called after a while with no MQTT (= no hope to be connected soon)
  virtual void onLongDisconnect() {
    if (_resetOnCommLoss) {
      setValue(_defaultValue);
    }
  }
  
  std::string toString() {
    // You may want to override this method.
    // Conversion to double solves patch::to_string not sending anything for ints
    return patch::to_string((double) *_pValue);
  }

  virtual void onMessage(const char *topic, const byte *payload, long length) {
    T newValue = parse(payload, length);
    setValue(newValue);
  }

  // This assumes the double type can accomodate everything we need in our basic T types
  // If not, you'll need to override this function
  virtual T parse(const byte *payload, long length) {
    return (T) patch::str2double(payload, length, (double) *_pValue);
  }

  virtual void increment(Object *other=NULL) { Serial.println("Oops.. unimplemented increment"); }
  virtual void decrement(Object *other=NULL) { Serial.println("Oops.. unimplemented decrement"); }
  virtual void reset    (Object *other=NULL) { setValue(_defaultValue);                          }
  virtual void setFrom  (Object *other=NULL) { Serial.println("Oops.. unimplemented set"      ); }

};


typedef Variable<bool  > BoolVar  ;
typedef Variable<uint8 > Uint8Var ;
typedef Variable<int   > IntVar   ;
typedef Variable<float > FloatVar ;


#endif // __VARIABLE_H__
